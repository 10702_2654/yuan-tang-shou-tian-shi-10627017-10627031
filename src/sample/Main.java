package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("圓堂手天使點餐系統v0.04");
        primaryStage.setScene(new Scene(root, 600, 600));
        primaryStage.show();
        //待加功能
        //預計送餐時間 餐點製作進度
    }

    public static void main(String[] args) {
        launch(args);
    }
}
