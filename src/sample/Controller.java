package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Font;

import java.awt.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Controller /*extends Application*/ {
    public int allfood = 7;
    public Tab Tab1;
    public Tab Tab2;
    public Button BtnOrder;
    public Label Cost;
    public int on = 1, cost = 0;
    public ListView<XCell> OrderList = new ListView<XCell>();
    public Alert alert;
    public Button BtnService;
    int cc = -1, del = 0;
    int[] fc = new int[30];
    int[] n = new int[30];
    String[] name = new String[30];
    public String RtnStr = "";
    ObservableList<String> strList = FXCollections.observableArrayList();
    ObservableList<String> strNULL = FXCollections.observableArrayList();

    List<XCell> CellList = new ArrayList<>();

    public String GETTIME() {
        int second = LocalDateTime.now().getSecond();
        int minute = LocalDateTime.now().getMinute();
        int hour = LocalDateTime.now().getHour();
        return (hour + ":" + minute + ":" + second);
    }

    public String GetValue() {
        RtnStr = "";
        for (int i = 0; i < allfood; i++) {
            if (fc[i] > 0) {
                RtnStr += name[i] + "x" + fc[i] + "\n";
            }
        }
        return RtnStr;
    }

    public class XCell extends ListCell<String> {
        HBox hbox = new HBox();
        Label label = new Label();
        Button BtnMinus = new Button("-");
        Label Foodcount = new Label();
        Button BtnPlus = new Button("+");


        XCell(String food, int price, int fn) {
            super();

            BtnMinus.setPrefSize(15, 15);
            BtnPlus.setPrefSize(15, 15);

            BtnMinus.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    if (fc[fn] > 1) {
                        fc[fn]--;
                        Foodcount.setText(Integer.toString(fc[fn]));
                        cost -= price;
                        Cost.setText("金額:" + cost);
                    } else {
                        CellList.remove(n[fn]);
                        fc[fn]--;
                        del++;
                        for (int i = 0; i < allfood; i++) {
                            if (n[i] > n[fn]) n[i]--;
                        }
                        cc--;
                        ObservableList<XCell> myList = FXCollections.observableList(CellList);
                        OrderList.setItems(myList);
                        OrderList.refresh();
                        cost -= price;
                        Cost.setText("金額:" + cost);
                    }
                }
            });

            BtnPlus.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    fc[fn]++;
                    Foodcount.setText(Integer.toString(fc[fn]));
                    cost += price;
                    Cost.setText("金額:" + cost);
                    //System.out.println(n[fn]+"GOOD");
                }
            });

            label.setText(food);
            label.setFont(Font.font(15));
            label.setMinWidth(160);
            Foodcount.setMinWidth(20);
            Foodcount.setAlignment(Pos.CENTER);
            Foodcount.setText(Integer.toString(fc[fn]));
            Foodcount.setFont(Font.font(15));

            hbox.getChildren().addAll(label, BtnMinus, Foodcount, BtnPlus);
            setGraphic(hbox);

        }
    }

    public int ADD(String sss, int costt, int fn) {
        cc++;
        CellList.add(new XCell(sss, costt, fn));
        ObservableList<XCell> myList = FXCollections.observableList(CellList);
        OrderList.setItems(myList);
        OrderList.refresh();
        cost += costt;
        Cost.setText("金額:" + cost);
        return cc;
    }

    public void PLUS(String sss, int costt, int fn) {
        CellList.set(n[fn], new XCell(sss, costt, fn));
        ObservableList<XCell> myList = FXCollections.observableList(CellList);
        OrderList.setItems(myList);
        OrderList.refresh();
        cost += costt;
        Cost.setText("金額:" + cost);
    }

    public void ClickFood0(MouseEvent mouseEvent) {
        fc[0]++;
        name[0] = "勁辣雞腿堡";
        if (fc[0] == 1) n[0] = ADD(name[0], 69, 0);
        else PLUS(name[0], 69, 0);
    }

    public void ClickFood1(MouseEvent mouseEvent) {
        fc[1]++;
        name[1] = "大麥克";
        if (fc[1] == 1) n[1] = ADD(name[1], 69, 1);
        else PLUS(name[1], 69, 1);
    }

    public void ClickFood2(MouseEvent mouseEvent) {
        fc[2]++;
        name[2] = "嫩煎雞腿堡";
        if (fc[2] == 1) n[2] = ADD(name[2], 79, 2);
        else PLUS(name[2], 79, 2);
    }

    public void ClickFood3(MouseEvent mouseEvent) {
        fc[3]++;
        name[3] = "麥香魚";
        if (fc[3] == 1) n[3] = ADD(name[3], 49, 3);
        else PLUS(name[3], 49, 3);
    }

    public void ClickFood4(MouseEvent mouseEvent) {
        fc[4]++;
        name[4] = "麥香雞";
        if (fc[4] == 1) n[4] = ADD(name[4], 49, 4);
        else PLUS(name[4], 49, 4);
    }

    public void ClickFood5(MouseEvent mouseEvent) {
        fc[5]++;
        name[5] = "雙層牛肉吉事堡";
        if (fc[5] == 1) n[5] = ADD(name[5], 59, 5);
        else PLUS(name[5], 59, 5);
    }

    public void ClickFood6(MouseEvent mouseEvent) {
        fc[6]++;
        name[6] = "安格斯黑牛堡";
        if (fc[6] == 1) n[6] = ADD(name[6], 99, 6);
        else PLUS(name[6], 99, 6);
    }

    public void ChkOrder(ActionEvent actionEvent) {
        if (CellList.size() > 0) {

            alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setContentText("確定送出訂單?");
            alert.setHeaderText("===訂餐明細===\n" + GetValue() + "\n總金額:" + cost);
            alert.showAndWait();
            ButtonType btntype = alert.getResult();
            if (btntype == ButtonType.OK) {
                System.out.println("======訂餐編號" + on + "======");
                System.out.println(GetValue());
                System.out.println(Cost.getText());
                System.out.println("訂餐時間:" + GETTIME());

                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText("訂單已送出!");
                alert.setContentText("訂餐時間:" + GETTIME());
                alert.showAndWait();

                for (int i = 0; i < cc + 2; i++) {
                    n[i] = 0;
                }
                cc = -1;
                CellList.clear();
                ObservableList<XCell> myList = FXCollections.observableList(CellList);
                OrderList.setItems(myList);

                java.util.Arrays.fill(fc, 0);
                on++;
                RtnStr = "";
                cost = 0;
                Cost.setText("金額:" + cost);
            }
        } else {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("購物車為空!");
            alert.showAndWait();
        }
    }

    public void CallService(ActionEvent actionEvent) {
        System.out.println("====XX桌需要服務====");
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("已通知 將盡速為您服務");
        alert.showAndWait();
    }
}
